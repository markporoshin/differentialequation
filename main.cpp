#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include "Runge/DiffRunge.hpp"
#include "PredictorСorrector/PredCorrector.hpp"

using namespace std;

void diffProblemMain(int mode);
static void readFromFile(int & M, dbl & alpha, dbl & a, dbl & b, dbl & y0, dbl & E, string const & name);
static void writeToFile(int N, vector<dbl> const & xh, vector<dbl> const & yh, string const & name);
static dbl f(dbl x, dbl  y = 0);
static dbl F(dbl x);
static bool checkAnswer(vector<dbl> const & yh, vector<dbl> const & xh, dbl E);





int main(int argv, char * args[]) {

    if (argv == 2)
        diffProblemMain(std::stoi(args[1]));
    else
        cout << "not enough params" << endl;

    return 0;
}






static dbl f(dbl x, dbl  y) {
    return y * y * exp(x) - 2 * y;
}
static dbl F(dbl x) {
    return exp(-x);
}
static void readFromFile(unsigned & M, dbl & alpha, dbl & a, dbl & b, dbl & y0, dbl & E, string const & name) {
    ifstream in(name);
    in >> M >> alpha >> a >> b >> y0 >> E;
}
static void writeToFile(int N, vector<dbl> const & xh, vector<dbl> const & yh, string const & name) {
    ofstream out(name);

    out  << std::setprecision(15) << xh.size() << endl;
    for (int i = 0; i < xh.size(); ++i) {
        out  << std::setprecision (15) << xh[i] << " " << yh[i] << endl;
    }
}
static bool checkAnswer(vector<dbl> const & yh, vector<dbl> const & xh, dbl E) {
    for (int i = 0; i < xh.size(); ++i) {
        if (fabs(yh[i] - F(xh[i])) > E) return false;
    }
    return true;
}




void diffProblemMain(int mode) {
    unsigned M;
    dbl E_N;
    dbl alpha, a, b, y0;
    string in = "/Users/mark/CLionProjects/differ/in.txt";
    string out = "/Users/mark/CLionProjects/differ/out.txt";


    readFromFile(M, alpha, a, b, y0, E_N, in);
    vector<dbl> xh;
    vector<dbl> yh;

    vector<dbl> grid(M+1);
    for (int i = 0; i < grid.size(); ++i)
        grid[i] = a + (b-a) * i / M;


    switch (mode) {
        case 0: {
            int N = 0;
            for (int j = 0; j < grid.size()-1; ++j) {
                DiffRunge solver(alpha, grid[j], grid[j+1], y0, f);
                pair<vector<dbl>, int> res = solver.byRungeRule(E_N);
                xh.insert(xh.end(), solver.xh.begin(), solver.xh.end());
                yh.insert(yh.end(), res.first.begin(), res.first.end());
                N += res.second;
                y0 = yh.back();
            }
            writeToFile(N, xh, yh, out);
        } break;
        case 1: {
            int N = 0;
            for (int j = 0; j < grid.size()-1; ++j) {
                DiffRunge solver(alpha, grid[j], grid[j+1], y0, f);
                vector<dbl> res = solver.solve((unsigned)E_N);
                xh.insert(xh.end(), solver.xh.begin(), solver.xh.end());
                yh.insert(yh.end(), res.begin(), res.end());
                N += (int)E_N;
                y0 = yh.back();
            }
            writeToFile(-1,   xh,   yh, out);
        } break;
        case 3: {
            int N = 0;
            for (int j = 0; j < grid.size()-1; ++j) {
                PredCorMthd solver(alpha, grid[j], grid[j+1], y0, f);
                pair<vector<dbl>, int> res = solver.byRungeRule(E_N);
                xh.insert(xh.end(), solver.xh.begin(), solver.xh.end());
                yh.insert(yh.end(), res.first.begin(), res.first.end());
                N += res.second;
                y0 = yh.back();
            }
            writeToFile(N, xh, yh, out);
        } break;
        case 2: {
            int N = 0;
            for (int j = 0; j < grid.size()-1; ++j) {
                PredCorMthd solver(alpha, grid[j], grid[j+1], y0, f);
                vector<dbl> res = solver.solve((unsigned)E_N);
                xh.insert(xh.end(), solver.xh.begin(), solver.xh.end());
                yh.insert(yh.end(), res.begin(), res.end());
                N += (int)E_N;
                y0 = yh.back();
            }
            writeToFile(-1,   xh,   yh, out);
        } break;
        default:
            cout << "unexpected mode (diffProblemMain)" << endl;
    }



}