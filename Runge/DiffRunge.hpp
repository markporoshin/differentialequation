//
// Created by Марк on 09.04.19.
//

#ifndef DIFFER_DIFF_RUNGE_HPP
#define DIFFER_DIFF_RUNGE_HPP

#include <vector>

using namespace std;

typedef long double dbl;

class DiffRunge {
    dbl a, b;
    dbl alpha;
    dbl y0;
    dbl (*fun)(dbl x, dbl y);
    dbl h;
    int N;
    void iter(int ind);

public:
    vector<dbl> xh;
    vector<dbl> yh;

public:
    DiffRunge(dbl alpha, dbl a, dbl b, dbl y0, dbl (*fun)(dbl x, dbl y));
    vector<dbl> const &  solve(int N);
    const pair<vector<dbl>, int> byRungeRule(dbl E);
};


void diffProblemMain(int mode);

#endif //DIFFER_DIFF_RUNGE_HPP
