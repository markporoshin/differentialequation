//
// Created by Марк on 09.04.19.
//

#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "DiffRunge.hpp"
#include "../PredictorСorrector/PredCorrector.hpp"






pair<vector<dbl>, int> const DiffRunge::byRungeRule(dbl E) {
    vector<dbl> yh_b;
    N = 2;
    yh = this->solve(N);
    do {
        yh_b = yh;
        N *= 2;
        yh = this->solve(N);
    } while ( (yh_b[yh_b.size()-1] - yh[yh.size()-1]) > E );
    return {yh, N};
}


void DiffRunge::iter(int ind) {
    dbl xi = xh[ind-1];
    dbl yi = yh[ind-1];


    yh.push_back(yi + h * (
          (1-alpha) * fun(xi, yi) +
            alpha * fun(xi + h / (2*alpha), yi + h / (2*alpha) * fun(xi, yi))));
}
DiffRunge::DiffRunge(dbl alpha, dbl a, dbl b, dbl y0, dbl (*fun)(dbl, dbl)) : alpha(alpha), fun(fun), a(a), b(b), y0(y0) {};
vector<dbl> const &  DiffRunge::solve(int N) {
    yh.clear();
    xh.clear();


    for (int i = 0; i < N; ++i)
        xh.push_back(a + (b-a) * i / (N-1));
    yh.push_back(y0);

    h = (b - a) / (N - 1);
    for (int i = 1; i < N; ++i)
        iter(i);
    return yh;
}