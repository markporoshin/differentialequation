//
// Created by Марк on 09.05.19.
//

#ifndef DIFFER_PREDCORRECTOR_HPP
#define DIFFER_PREDCORRECTOR_HPP

#include <vector>

using namespace std;

typedef long double dbl;

class PredCorMthd {
    dbl (*fun)(dbl x, dbl y);
    dbl a;
    dbl b;
    dbl alpha;
    dbl y0;

public:
    vector<dbl> xh;
    vector<dbl> yh;

public:
    PredCorMthd(dbl alpha, dbl a, dbl b, dbl y0, dbl (*fun)(dbl x, dbl y));
    vector<dbl> const & solve(int N);
    const pair<vector<dbl>, int> byRungeRule(dbl E);
};


#endif //DIFFER_PREDCORRECTOR_HPP
