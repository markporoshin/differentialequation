//
// Created by Марк on 09.05.19.
//

#include "PredCorrector.hpp"
#include "../Runge/DiffRunge.hpp"


vector<dbl> const & PredCorMthd::solve(int N) {
    DiffRunge predictor(alpha, a, b, y0, fun);
    predictor.solve(N);
    xh = predictor.xh;
    yh = predictor.yh;

    dbl h = (b - a) / (N - 1);
    for (int i = 0; i < xh.size() - 1; ++i) {
        yh[i+1] = yh[i] + ((dbl)h) / 2 * (fun(xh[i+1], (dbl)yh[i+1]) + fun(xh[i], yh[i]));
    }

    return yh;
}


const pair<vector<dbl>, int> PredCorMthd::byRungeRule(dbl E) {
    vector<dbl> yh_b;
    int N = 2;
    yh = this->solve(N);
    do {
        yh_b = yh;
        N *= 2;
        yh = this->solve(N);
    } while ( (yh_b[yh_b.size()-1] - yh[yh.size()-1]) > E );
    return {yh, N};
}

PredCorMthd::PredCorMthd(dbl alpha, dbl a, dbl b, dbl y0, dbl (*fun)(dbl, dbl)) : alpha(alpha), fun(fun), a(a), b(b), y0(y0) {};